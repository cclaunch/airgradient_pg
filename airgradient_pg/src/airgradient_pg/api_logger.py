#!/usr/bin/env python3
"""Application for loggin Airgradient sensors to a PostgreSQL database."""

import argparse
import signal
import sys
import time
import requests
from airgradient_pg.json2postgres import Json2Postgres


def signal_handler(_signal, _frame):
    """Signal handler."""
    sys.exit(0)


def main():
    """Main application to run the logger."""
    signal.signal(signal.SIGINT, signal_handler)

    parser = argparse.ArgumentParser(prog="logger", description="log Airgradient sensor data to postgresql database")
    parser.add_argument("-l", "--location", required=True, nargs="*", help="location id to query. multiple may be provided")
    parser.add_argument("-t", "--token", required=True, help="API token")
    parser.add_argument("-p", "--period", default=10.0, type=float, help="period at which to query the API for new data")
    parser.add_argument("--db-table", default="airgradient_data", help="database table to write to")
    args = parser.parse_args()

    start_time = time.monotonic()
    ag_db = Json2Postgres(table=args.db_table)

    print(f"started api_logger with args: {args}")

    while True:
        for loc in args.location:
            api_url = f"https://api.airgradient.com/public/api/v1/locations/{loc}/measures/current?token={args.token}"
            resp = requests.get(url=api_url, timeout=10.0)
            if resp.status_code == 200:
                ag_db.insert_from_json(resp.json())
            else:
                print(f"error retrieving location: {loc}: {resp.status_code}, {resp.reason}")

        # Helps keep accurate loop timing https://stackoverflow.com/a/25251804/1544725
        time.sleep(args.period - ((time.monotonic() - start_time) % args.period))


if __name__ == "__main__":
    main()
