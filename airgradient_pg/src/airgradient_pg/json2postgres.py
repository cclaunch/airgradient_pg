"""Insert json formatted Airgradient data into a PostgreSQL database."""

import psycopg2


class Json2Postgres:
    """Object to hold open a PostgreSQL connection and create/insert a table with Airgradient data."""

    def __init__(self, table):
        self.conn = None
        self.cur = None
        self.table = table
        self.insert_sql = f"INSERT INTO {self.table} \
            (timestamp, locationid, pm01, pm02, pm10, pm003count, atmp, rhum, rco2, tvoc, tvocindex, noxindex, wifi) \
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

    def check_conn(self):
        """Check the database connection and cursor."""
        if not self.conn or self.conn.closed:
            self.connect()
        if not self.cur or self.cur.closed:
            self.get_cursor()

    def connect(self):
        """Connect to the database."""
        self.conn = psycopg2.connect()

    def get_cursor(self):
        """Get a cursor to execute SQL commands."""
        self.cur = self.conn.cursor()

    def create_table(self):
        """Create the table to store the Airgradient data."""
        self.check_conn()
        sql = f'CREATE TABLE IF NOT EXISTS {self.table} \
            ("timestamp" timestamp without time zone NOT NULL, \
            locationid integer NOT NULL, \
            pm01 integer, \
            pm02 integer, \
            pm10 integer, \
            pm003count integer, \
            atmp double precision, \
            hum integer, \
            rco2 integer, \
            tvoc double precision, \
            tvocindex integer, \
            noxindex integer, \
            wifi integer, \
            CONSTRAINT {self.table}_pkey PRIMARY KEY ("timestamp", locationid))'
        self.cur.execute(sql)
        self.conn.commit()

    def insert_from_json(self, json_data):
        """Insert the json data, assuming the format is correct."""
        self.check_conn()
        try:
            self.cur.execute(
                self.insert_sql,
                (
                    json_data["timestamp"],
                    json_data["locationId"],
                    json_data["pm01"],
                    json_data["pm02"],
                    json_data["pm10"],
                    json_data["pm003Count"],
                    json_data["atmp"],
                    json_data["rhum"],
                    json_data["rco2"],
                    json_data["tvoc"],
                    json_data["tvocIndex"],
                    json_data["noxIndex"],
                    json_data["wifi"],
                ),
            )
        except psycopg2.errors.UniqueViolation:
            # ignore duplicate inserts
            pass
        except psycopg2.errors.UndefinedTable:
            self.conn.rollback()
            self.create_table()

        self.conn.commit()
