FROM python:3.8-slim
ENV API_TOKEN "/path/to/api/token"
ENV API_LOCATION "/path/to/location/file"
ENV PYTHONUNBUFFERED=1
COPY ./entrypoint.sh /
COPY ./airgradient_pg /root/airgradient_pg
SHELL ["/bin/bash", "-c"]
WORKDIR /root/airgradient_pg
RUN pip3 install --no-cache-dir .
ENTRYPOINT [ "/entrypoint.sh" ]
CMD python3 /root/airgradient_pg/src/airgradient_pg/api_logger.py -l $(cat ${API_LOCATION}) -t $(cat ${API_TOKEN})
