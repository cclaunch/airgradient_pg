# airgradient_pg

A Python application to gather Aigradient sensor data via the API and write to a PostgreSQL database.

## Setup

The quickest way to get up and running is to use the docker compose file along with the secrets files.

```
  airgradient_passfile:
    file: /home/chuck/.secrets/airgradient_passfile
  airgradient_token:
    file: /home/chuck/.secrets/airgradient_token
  airgradient_location:
    file: /home/chuck/.secrets/airgradient_location
```

The included docker compose file includes these three secrets.  Change the file paths to be a plain text file with each of the given items.  Passfile follows the PostgreSQL definition of a password file as defined [here](https://www.postgresql.org/docs/current/libpq-pgpass.html#:~:text=The%20file%20.,%25APPDATA%25%5Cpostgresql%5Cpgpass.).  `hostname:port:database:username:password` as of this writing.   
   
The token file is the API token provided by the Airgradient device.   
   
The location file is a space separated list of location IDs to be queried via the Airgradient API.  Multiple location IDs is supported but untested as I only have one device currently.  It's up to the user to create additional tables which identify their location IDs with a description.

Setting these three files and the running the docker compose should result all data being written to a table called `airgradient_data` by default.

`TL;DR`

* Run `docker compose up -d`

## Manually Running the Python Script

The user may choose to manually run the `api_logger.py` from this repository.  This also works, assuming the user has used `pip3` to install the proper dependencies from the `pyproject.toml`.  The `--help` argument will provide the command line arguments necessary to run the script.

## Notes

* The application will automatically create the table if it does not exist.
